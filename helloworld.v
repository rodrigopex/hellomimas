`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:51:51 12/21/2016 
// Design Name: 
// Module Name:    helloworld 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module helloworld(input a, b, c, d, output [7:0] e);

assign e = {4'b0000, ~d, ~c, ~b, ~a};

endmodule
