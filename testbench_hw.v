`timescale 1ns / 1ns

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:18:24 01/02/2017
// Design Name:   helloworld
// Module Name:   /home/rodrigopex/Projects/FPGA/xilinx/HelloWorld/testbench_hw.v
// Project Name:  HelloWorld
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: helloworld
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module testbench_hw;

	// Inputs
	reg a;
	reg b;

	// Outputs
	wire c;

	// Instantiate the Unit Under Test (UUT)
	helloworld uut (
		.a(a), 
		.b(b), 
		.c(c)
	);

	initial begin
		// Initialize Inputs
		a = 0;
		b = 0;
		
		// Wait 100 ns for global reset to finish
		#100;
		#30 a = 1;
		#60 b = 1;
        
		// Add stimulus here
	end
      
endmodule

